VERSION = (0, 5, 3,)
__version__ = '.'.join(map(str, VERSION))
default_app_config = 'local_messages.apps.DjangoMessagesConfig'
