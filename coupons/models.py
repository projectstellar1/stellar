# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth import get_user_model

# Create your models here.

def coupon_code_generator():
    ''' Creates a random coupon code 
    '''
    from base64 import b32encode
    from hashlib import sha1
    from random import random
    rude = ('lol',)
    bad_pk = True
    while bad_pk:
        pk = b32encode(sha1(str(random())).digest()).lower()[:10]
        bad_pk = False
        for rw in rude:
            if pk.find(rw) >= 0: bad_pk = True
    return pk

class Coupon (models.Model):
    code = models.CharField(
        max_length = 10,
        default = coupon_code_generator
    )
    user = models.ForeignKey(
        get_user_model(),
    )
    dt_created = models.DateField(
        auto_now_add=True
    )
    dt_used = models.DateField(
        null=True,
        blank=True
    )
    coupon_type = models.CharField(
        max_length=30,
        blank=True,
        null=True
    )

    def __str__(self):
        return u'%s - %s' % (self.user, self.code)