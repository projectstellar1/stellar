from random import randint

from django.core.management.base import BaseCommand, CommandError

from django.contrib.sites.models import Site

class Command(BaseCommand):
    help = 'Populates the database with startup data'

    def handle(self, *args, **options):
        try:
            site = Site.objects.get(id=1)
            site.domain = "stella.hexiawebservices.co.uk"
            site.name = 'Stella'
            site.save()
            print ("Site set to Stella, stella.hexiawebservices.co.uk")
        except:
            print ("Something went wrong - site not found")