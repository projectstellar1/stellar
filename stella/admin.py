from django.contrib import admin
from django.contrib.auth.models import User
from registration.models import RegistrationProfile
from models import UserProfile, ValidEmail, Institution, NewInstitution, DirtyName, Help, ReportListing, SafeExchange

from models import Category, Category2, Category3, Amenity,  Listing
from extras.admin import institution_download, my_stripe
from filters import DropdownFilter

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']
    ordering = ['name']
admin.site.register(Category, CategoryAdmin)
    
class AmenityAdmin(admin.ModelAdmin):
    list_display = ['name',]
    ordering = ['name']
#admin.site.register(Amenity, AmenityAdmin)


class NewInstitutionAdmin(admin.ModelAdmin):
    actions = [institution_download.download_institutions,]
admin.site.register(NewInstitution, NewInstitutionAdmin)

class ReportListingAdmin(admin.ModelAdmin):
    list_display = ['listing','user']
    ordering = ['listing','user']
admin.site.register(ReportListing, ReportListingAdmin)

class ListingAdmin(admin.ModelAdmin):
    list_filter = (('user__userprofile__institution__name',DropdownFilter),('user__username', DropdownFilter))
    search_fields = ('user__first_name','user__last_name','user__username','user__email')

    def lookup_allowed(self, key, value):
        return True
    
admin.site.register(Listing, ListingAdmin)


admin.site.register(Category2)
admin.site.register(Category3)
admin.site.register(Institution)
admin.site.register(ValidEmail)
admin.site.register(DirtyName)
admin.site.register(Help)
admin.site.register(SafeExchange)



class UserProfileAdmin(admin.ModelAdmin):
    list_filter = ('verified',)
admin.site.register(UserProfile, UserProfileAdmin)