'''
Auto messages has been disabled in settings.py
This code now sends a message depending on User preferences
'''


from django.db.models import signals
from django.dispatch import receiver
from django.contrib.auth import get_user_model
from django.conf import settings

from local_messages.utils import new_message_email
from local_messages.models import Message

from .models import UserProfile, Listing

from coupons.models import Coupon
from djstripe.models import Customer
import stripe

USER_MODEL = get_user_model()

def check_user_email_notification(sender, instance, **kwargs):
    if instance.recipient.userprofile.email_notification:
        signals.post_save.connect(new_message_email, sender=Message)
    else:
        pass
    
signals.post_save.connect(check_user_email_notification, sender=Message)

@receiver(signals.post_save, sender=get_user_model())
def initial_coupon(sender, instance, created, **kwargs):
    ''' Creates an initial coupon for a customer 
    '''
    if created:
        c = Coupon (
            user = instance,
            coupon_type = "Signup Coupon"
        )
        c.save()

@receiver(signals.post_save, sender=UserProfile)
def create_stripe_customer(sender, instance, created, **kwargs):
    ''' Creates a stripe account for a new signup
    '''
    if created:
        stripe.api_key = settings.STRIPE_SECRET_KEY
        
        customer = stripe.Customer.create(
            email=instance.user.email
        )
        instance.stripe_id = customer['id']
        instance.save()
        
@receiver(signals.post_save, sender=Customer)
def link_djstripe_customer(sender, instance, created, **kwargs):
    ''' Links a new dj_stripe customer with a user based on the Stripe return
    '''
    if created:
        user = USER_MODEL.objects.get(userprofile__stripe_id = instance.stripe_id)
        instance.subscriber = user
        instance.save()