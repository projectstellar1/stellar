<script>
var $divs = $("div.item");
$('#newestBtn').on('click', function () {
    var OrderedDivs = $divs.sort(function (a, b) {
        return parseInt($(a).find("div.item-id").text()) < parseInt($(b).find("div.item-id").text());
    });
    $("#all-my-items").html(OrderedDivs);
});

$('#oldestBtn').on('click', function () {
    var OrderedDivs = $divs.sort(function (a, b) {
        return parseInt($(a).find("div.item-id").text()) > parseInt($(b).find("div.item-id").text());
    });
    $("#all-my-items").html(OrderedDivs);
});


$('#lowestBtn').on('click', function () {
    var OrderedDivs = $divs.sort(function (a, b) {
        return parseFloat($(a).find("span.price").text()) > parseFloat($(b).find("span.price").text());
    });
    $("#all-my-items").html(OrderedDivs);
});

$('#highestBtn').on('click', function () {
    var OrderedDivs = $divs.sort(function (a, b) {
        return parseFloat($(a).find("span.price").text()) < parseFloat($(b).find("span.price").text());
    });
    $("#all-my-items").html(OrderedDivs);
});

function sortItems() {
    var selbox = document.getElementById("sortSelection");
    var idx = selbox.selectedIndex;
    var sortValue = selbox.options[idx].value;
    if (sortValue == 1) {
        var OrderedDivs = $divs.sort(function (a, b) {
            return parseInt($(a).find("div.item-id").text()) < parseInt($(b).find("div.item-id").text());
        });
    }
    if (sortValue == 2) {
        var OrderedDivs = $divs.sort(function (a, b) {
            return parseInt($(a).find("div.item-id").text()) > parseInt($(b).find("div.item-id").text());
        });
    }
    if (sortValue == 3) {
        var OrderedDivs = $divs.sort(function (a, b) {
            return parseFloat($(a).find("span.price").text()) > parseFloat($(b).find("span.price").text());
        });
    }
    if (sortValue == 4) {
        var OrderedDivs = $divs.sort(function (a, b) {
            return parseFloat($(a).find("span.price").text()) < parseFloat($(b).find("span.price").text());
        });
    }
    
    
    $("#all-my-items").html(OrderedDivs);
}
</script>