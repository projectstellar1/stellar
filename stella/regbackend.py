''' This backend is to ensure institutions match the users email address '''


from models import UserProfile, Institution
from forms import UserRegForm

def user_created(sender, user, request, **kwargs):
    form = UserRegForm(request.POST)
    try: data = UserProfile.objects.get(user=user)
    except: data = UserProfile(user=user)
    data.institution = Institution.objects.get(id=int(form.data["institution"]))
    data.save()

from registration.signals import user_registered
user_registered.connect(user_created)