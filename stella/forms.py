from django.contrib.auth.models import User

from stella.models import Listing, Review

from nocaptcha_recaptcha.fields import NoReCaptchaField
from django import forms

from registration.forms import RegistrationForm, RegistrationFormUniqueEmail, RegistrationFormTermsOfService
from django.forms import ModelForm
from models import UserProfile, Institution, ValidEmail, NewInstitution, Category, Category2, Category3, DirtyName

from parameters import CONDITION_OPTIONS

class UserRegForm(RegistrationFormUniqueEmail, RegistrationFormTermsOfService):
    CHOICES = Institution.objects.all().order_by('name')
    
    institution=forms.ChoiceField(
        choices=( (x.id, x.name) for x in CHOICES ),
        required = True)

    referral_code = forms.CharField(
        required = False
    )
   
    def clean(self):
        """Check if valid email for institution."""
        super(UserRegForm, self).clean()
        cleaned_data = self.cleaned_data
        
        try: email = cleaned_data['email']
        except:
            msg = forms.ValidationError("Email already used")
            self.add_error('email',msg)
            email = ''
        myinstitution = Institution.objects.get(id=int(cleaned_data.get('institution')))
        valid_emails = ValidEmail.objects.filter(institution=myinstitution)
        
        matched = False
        for e in valid_emails:
            if e.extension in email: matched=True
            
        if not matched:
            msg = forms.ValidationError("We do not recognise this email for %s" % myinstitution.name)
            self.add_error('email', msg)
            
        return self.cleaned_data

class InstitutionForm(forms.ModelForm):
    class Meta:
        model = NewInstitution
        fields = ['email','institution']
        widgets = {
            'email' : forms.TextInput(attrs={'class' : 'form-control',
                                             'placeholder' : 'Your email at the school'}),
            'institution' : forms.TextInput(attrs={'class' : 'form-control',
                                                   'placeholder' : 'School name'})
        }

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ['user','feedback','rating']
        widgets = {
            'user' : forms.HiddenInput(),
            'feedback' : forms.Textarea(attrs={'class': 'form-control',
                                                'placeholder' : 'Feedback'}),
            'rating' : forms.RadioSelect(attrs={'class' : 'input-rating'})
        }
        
class ContactForm(forms.Form):
    name = forms.CharField(
        min_length = 5,
        max_length = 60,
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'placeholder' : 'Name'}),
        label = "",
        required=True
    )
    subject = forms.CharField(
        min_length = 5,
        max_length = 60,
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'placeholder' : 'Subject'}),
        label = "",
        required=True)
    email = forms.EmailField(
        required=True,
        label = "",
        widget=forms.EmailInput(attrs={'class' : 'form-control',
                                       'placeholder' : 'Email address'})
        )
    message = forms.CharField (
        label = '',
        min_length = 10,
        max_length = 500,
        widget=forms.Textarea(attrs={'class': 'form-control',
                                     'placeholder' : 'Message'}),
    )
    contact_recaptcha = NoReCaptchaField(
        label = ''
    )
    
class UserForm (forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name','last_name']
        widgets = {
            'first_name' : forms.TextInput(attrs={'class': 'form-control',
                                                  'placeholder' : 'First Name',
                                                  'required' : 'True'}),
            'last_name' : forms.TextInput(attrs={'class': 'form-control',
                                                 'placeholder' : 'Last Name',
                                                  'required' : 'True'}),

        }
        
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = "First name"
        self.fields['last_name'].label = "Last name"
        
    def clean(self):
        super(UserForm, self).clean()
        cleaned_data = self.cleaned_data
        
        dirty_names = DirtyName.objects.all()
        first_name = cleaned_data['first_name'].lower()
        last_name = cleaned_data['last_name'].lower()
        
        for d in dirty_names:
            if first_name == d.name:
                msg = forms.ValidationError("%s flagged as inappropriate name." % d.name)
                self.add_error('first_name', msg)
            if last_name == d.name:
                msg = forms.ValidationError("%s flagged as inappropriate name." % d.name)
                self.add_error('last_name', msg)
                
        return self.cleaned_data
        
class UserProfileForm (forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['telephone','email_notification','avatar']
        exclude =['id','institution','user','verified']
        widgets = {
            'telephone' : forms.TextInput(attrs={'class' : 'form-control',
                                                 'placeholder' : 'Contact Phone Number'}),
            'avatar' : forms.FileInput (attrs={'class' : 'btn-primary center',
                                               'accept': 'image/*;capture=camera',
                                               'onchange' : 'readURL(this);'}),
            'email_notification' : forms.CheckboxInput(attrs={'class' : '',
                                                              'style' : 'display:block'}),
        }
        
from local_messages.forms import ComposeForm
class MyComposeForm(ComposeForm):
    def __init__(self, *args, **kwargs):
        super(MyComposeForm, self).__init__(*args, **kwargs)
        self.fields['recipient'].widget.attrs.update({'class' : 'form-control'})
        self.fields['subject'].widget.attrs.update({'class' : 'form-control'})
        self.fields['body'].widget.attrs.update({'class' : 'form-control'})


class MyReplyForm(MyComposeForm):
    def __init__(self, *args, **kwargs):
        super(MyReplyForm, self).__init__(*args, **kwargs)
        self.fields['recipient'].widget.attrs.update({'readonly' : True})
        self.fields['subject'].widget.attrs.update({'readonly' : True})

class AdvancedSearchForm(forms.Form):
    CATEGORY_OPTIONS = Category.objects.all().order_by('primary')
    CATEGORY2_OPTIONS = Category2.objects.all().order_by('name')  
    CATEGORY3_OPTIONS = Category3.objects.all().order_by('name')
    keywords = forms.CharField(
        max_length = 60,
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'placeholder' : 'Keywords',}),
        label = "",
        required = False,
    )
    min_price = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'placeholder' : 'Min Price',
                                      'min' : 0}),
        label = '',
        required = False,
    )
    max_price = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'placeholder' : 'Max Price',
                                      'min' : 0}),
        label = '',
        required = False,
    )
    try: categoryAS = forms.ChoiceField(
        widget = forms.Select(attrs={'class': 'form-control',
                                     "onchange" : "changeCategory('AS');",
                                     'placeholder' : 'Select Category',}),
        label = '',
        required = False,
        choices = ((x.id, x.primary) for x in CATEGORY_OPTIONS),
        )
    except: pass
    try: category2AS = forms.ChoiceField(
        widget = forms.Select(attrs={'class': 'form-control',
                                     'onchange' : 'changeCategory2("AS");'}),
        label = '',
        required = False,
        choices = ((x.id, x.name) for x in CATEGORY2_OPTIONS),
        )
    except:pass
        
    try: category3AS = forms.ChoiceField(
        widget = forms.Select(attrs={'class': 'form-control',}),
        label = '',
        required = False,
        choices = ((x.id, x.name) for x in CATEGORY3_OPTIONS),
        )
    except:pass
    
    condition = forms.ChoiceField(
        widget = forms.Select(attrs={'class': 'form-control',
                                     'id' : 'id_conditionAS'}),
        label = '',
        choices = CONDITION_OPTIONS,
        required = False,
        )
        
    def __init__ (self, *args, **kwargs):
        super(AdvancedSearchForm, self).__init__(*args, **kwargs)
        
class LeftSearchForm(forms.Form):
    CATEGORY_OPTIONS = Category.objects.all().order_by('primary')
    CATEGORY2_OPTIONS = Category2.objects.all().order_by('name')
    CATEGORY3_OPTIONS = Category3.objects.all().order_by('name')
    keywords = forms.CharField(
        max_length = 60,
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'placeholder' : 'Keywords',
                                      'id' : 'id_keywordsLM'}),
        label = "",
        required = False,
    )
    min_price = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'placeholder' : 'Min Price',
                                      'min' : 0,
                                      'id' : 'id_min_priceLM'}),
        label = '',
        required = False,
    )
    max_price = forms.IntegerField(
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'placeholder' : 'Max Price',
                                      'min' : 0,
                                      'id' : 'id_max_priceLM'}),
        label = '',
        required = False,
    )
    try: categoryAS = forms.ChoiceField(
        widget = forms.Select(attrs={'class': 'form-control',
                                     'onchange' : 'changeCategory("LM");',
                                     'id' : 'id_categoryLM'}),
        label = '',
        required = False,
        choices = ((x.id, x.primary) for x in CATEGORY_OPTIONS),
        )
    except: pass
    try: category2AS = forms.ChoiceField(
        widget = forms.Select(attrs={'class': 'form-control',
                                     'onchange' : 'changeCategory2("LM");',
                                     'id' : 'id_category2LM'}),
        label = '',
        required = False,
        choices = ((x.id, x.name) for x in CATEGORY2_OPTIONS),
        )
    except:pass
        
    try: category3AS = forms.ChoiceField(
        widget = forms.Select(attrs={'class': 'form-control',
                                     'id' : 'id_category3LM'}),
        label = '',
        required = False,
        choices = ((x.id, x.name) for x in CATEGORY3_OPTIONS),
        )
    except:pass
        
    condition = forms.ChoiceField(
        widget = forms.Select(attrs={'class': 'form-control',
                                     'id' : 'id_conditionLM'}),
        label = '',
        choices = CONDITION_OPTIONS,
        required = False,
        )

    
    def __init__ (self, *args, **kwargs):
        super(LeftSearchForm, self).__init__(*args, **kwargs)

class BrowseForm(forms.Form):
    CATEGORY_OPTIONS = Category.objects.all().order_by('primary')
    CATEGORY2_OPTIONS = Category2.objects.all().order_by('name')
    CATEGORY3_OPTIONS = Category3.objects.all().order_by('name')
    
    try:categoryAS = forms.ChoiceField(
        widget = forms.Select(attrs={'class': 'form-control',
                                     'onchange' : 'changeCategory("LM");',
                                     'id' : 'id_categoryLM'}),
        label = '',
        required = False,
        choices = [[0, '----------']] + [[x.id, x.primary] for x in CATEGORY_OPTIONS],
        )
    except:pass
    try: category2AS = forms.ChoiceField(
        widget = forms.Select(attrs={'class': 'form-control',
                                     'onchange' : 'changeCategory2("LM");',
                                     'id' : 'id_category2LM'}),
        label = '',
        required = False,
        choices = [[0, '----------']] + [[x.id, x.name] for x in CATEGORY2_OPTIONS],
        )
    except:pass
        
    try: category3AS = forms.ChoiceField(
        widget = forms.Select(attrs={'class': 'form-control',
                                     'id' : 'id_category3LM'}),
        label = '',
        required = False,
        choices = [[0, '----------']] + [[x.id, x.name] for x in CATEGORY3_OPTIONS],
        )
    except:pass
    
    def __init__ (self, *args, **kwargs):
        super(BrowseForm, self).__init__(*args, **kwargs)