from django.apps import AppConfig
 
 
class StellaConfig(AppConfig):
    name = 'stella'
    verbose_name = 'Stella Application'
 
    def ready(self):
        import stella.signals