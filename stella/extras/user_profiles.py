from django.views.generic import UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from django.shortcuts import redirect
from django.conf import settings
from django.contrib import messages

import stripe

from djstripe.models import Card, Customer

class AddCreditCard(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    fields = ['id']

    def form_valid(self, *args, **kwargs):
        card_token = Card.create_token (
            number=self.request.POST['cardNumber'],
            exp_month = int(self.request.POST['expiryMonth']),
            exp_year = int(self.request.POST['expiryYear']),
            cvc = self.request.POST['cvv'],
            )
        stripe.api_key = settings.STRIPE_SECRET_KEY
        try:
            djstripe_customer = Customer.objects.get(subscriber=self.request.user)
            customer = stripe.Customer.retrieve(djstripe_customer.stripe_id)
        except:
            customer = stripe.Customer.create(
                email = self.object.email
            )
            up = self.object.userprofile
            up.stripe_id = customer['id']
            up.save()
        
        customer.sources.create(source=card_token)
        messages.success(self.request, "Card details saved")
        return redirect('goto-profile')