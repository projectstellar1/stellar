from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView
from django.http import HttpResponseForbidden, HttpResponseNotFound
from django.contrib.auth.models import User

import datetime
from django.utils import timezone

from django.views.decorators.csrf import csrf_exempt
import stripe
from django.template.loader import render_to_string
from django.core.mail import EmailMessage

from stella.models import *
from stella import settings
from stella.forms import ReviewForm

from nocaptcha_recaptcha.fields import NoReCaptchaField
from django import forms
from coupons.models import Coupon
from djstripe.models import Customer, Card

class FileFieldForm(forms.Form):
    file_field = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}),
                                 required = False)

class ReviewRequestForm (forms.Form):
    username = forms.CharField(
        max_length=30,
        widget = forms.TextInput(
            attrs={'class' : 'form-control',
                   'placeholder' : 'Username/Email'}
        ),
        label = "",
    )
    
    def clean(self):
        super(ReviewRequestForm, self).clean()
        cleaned_data = self.cleaned_data
        
        username = cleaned_data['username']
        matched = False
        users = User.objects.all()
        for u in users:
            if username == u.username:
                matched = True
                break
            
        if not matched:
            msg = forms.ValidationError("Cannot find user with username/email %s." % username)
            self.add_error('username', msg)
        
        return self.cleaned_data
    
class ListingForm(forms.ModelForm):
    recaptcha = NoReCaptchaField()
    '''
    safe_exchange = forms.BooleanField(
        required = False
    )
    '''
    
    class Meta:
        model = Listing
        fields = ['id','user','name','category','category2','category3','description','price','condition',
                  'image','tandc','phone_contact','email_contact','featured']
        widgets = {
            'id' : forms.HiddenInput(),
            'user' : forms.HiddenInput(),
            'name' : forms.TextInput(attrs={'class': 'form-control',
                                            'placeholder': 'Listing Title'}),
            'category' : forms.Select(attrs={'class': 'form-control',
                                             'onchange' : 'changeCategory("");'}),
            'category2' : forms.Select(attrs={'class': 'form-control',
                                              'onchange' : 'changeCategory2("");'}),
            'category3' : forms.Select(attrs={'class': 'form-control'}),
            'condition' : forms.Select(attrs={'class': 'form-control'}),
            'description' : forms.Textarea(attrs={'class': 'form-control',}),
            'price' : forms.TextInput(attrs={'class': 'form-control',
                                             'placeholder': 'Price'}),
            'image' : forms.FileInput(attrs={'class': 'form-control',
                                             'id' : 'input-file'}),
            }
        
    def __init__ (self, *args, **kwargs):
        super(ListingForm, self).__init__(*args, **kwargs)
        self.fields["category"].queryset = Category.objects.all().order_by('name')
        self.fields["category2"].queryset = Category2.objects.all().order_by('name')
        self.fields["category3"].queryset = Category3.objects.all().order_by('name')
        
    def clean(self):
        super(ListingForm, self).clean()
        cleaned_data = self.cleaned_data
        print cleaned_data
        try:
            cat2 = cleaned_data['category2']
            if cat2 == None or cat2 == 0: cleaned_data.pop('category2')
        except: pass
        try:
            cat3 = cleaned_data['category3']
            if cat3 == None: cleaned_data.pop('category3')
        except: pass
        
        return self.cleaned_data

def buy_subscription (request, listing_id):
    listing = Listing.objects.get(id=listing_id)
    variables = {}
    variables['listing'] = listing
    variables['stripe_key'] = settings.STRIPE_PUBLISH_KEY
    variables['stripe_currency'] = settings.STRIPE_CURRENCY
    variables['pricelist'] = PriceList.objects.filter(listing_type=listing.type).order_by('priority')
    return render(request, 'stella/pricing.html', variables)

@login_required
def add_listing(request, listing_id=0):
    variables = {}
    if listing_id == 0:
        form_data = {'user':request.user}
        form = ListingForm(initial=form_data)
        file_form=FileFieldForm()
        listing = None
        file_form_data = None
    else:
        listing = Listing.objects.get(id=listing_id)
        form = ListingForm(instance=listing)
        image_list = ListingImages.objects.filter(listing=listing)
        file_form_data = {'file_field' : image_list}
        file_form=FileFieldForm(initial=file_form_data)
        
    if request.method == "POST":
        form = ListingForm(request.POST, request.FILES, instance=listing)
        file_form = FileFieldForm(request.POST, request.FILES)
        if form.is_valid() and file_form.is_valid():
            listing = form.save()
            if listing.featured: listing.set_expires()
            files = request.FILES.getlist('file_field')
            for f in files:
                m = ListingImages (
                    image = f,
                    listing = listing
                )
                m.save()
            messages.success(request, 'Listing successfully added/updated.')
            variables['listing'] = listing
            variables['images'] = ListingImages.objects.filter(listing=listing)
            return render(request, 'stella/listing-detail.html', variables)
    
    variables['text'] = SafeExchange.objects.all().order_by('order')
    variables['listing'] = listing   
    variables['listing_images'] = ListingImages.objects.filter(listing=listing)         
    variables['form'] = form
    variables['file_form'] = file_form
    variables['settings'] = settings
    return render(request, 'stella/listing-submit.html', variables)

def process_payment(request, listing_id):
    #check for a valid credit card
    user = request.user
    try:
        customer = Customer.objects.get(subscriber=user)
    except:
        raise Exception("Process Payment: Cannot find djstripe Customer")
    cards = Card.objects.filter(customer=customer)
    if len(cards) < 1:
        messages.success(request, "You must add a valid created card to your account")
        return redirect('goto-profile')

    listing = Listing.objects.get(id=listing_id)
    stripe.api_key = settings.STRIPE_SECRET_KEY     
    
    #does user have an unused coupon
    if request.user.userprofile.unused_coupon():
        coupon = Coupon.objects.filter(user=request.user, dt_used__isnull=True)[0]
        coupon.dt_used = timezone.now()
        coupon.save()
        stripe.InvoiceItem.create(
            customer = customer.stripe_id,
            amount = -int(.99*100),  # amount in cents, again
            currency=settings.STRIPE_CURRENCY,
            description = "Coupon: " + coupon.code,
            )

    try:
        stripe.InvoiceItem.create(
            customer = customer.stripe_id,
            amount = int(.99*100),  # amount in cents, again
            currency=settings.STRIPE_CURRENCY,
            description= listing.name,
            )
        invoice = stripe.Invoice.create(
            customer = customer.stripe_id,
            auto_advance=True
        )


    except stripe.error.CardError, e:
        # The card has been declined
        return render(request, 'payment/payment_declined.html',
                      {'e' : e})

    listing.token = invoice['id']
    listing.featured = False
    listing.set_expires()
    listing.live=True
    listing.save()

    #send confirmation email
    msg = render_to_string('payment/confirm_email.txt',
                           {'contact': request.user,
                            'listing' : listing,})
    
    email = EmailMessage("Listing Confirmation",
                         msg,
                         to=[request.user.email])
    email.send()

    return render(request, 'payment/payment_confirmed.html')

@login_required
def make_live(request, listing_id):
    variables={}
    listing = Listing.objects.get(id=listing_id)
    listing.live = -(listing.live-1)
    listing.save()
    if listing.live: messages.success(request, 'Listing put live.')
    else: messages.success(request, 'Listing taken down.')
    variables['listing'] = listing
    variables['images'] = ListingImages.objects.filter(listing=listing)
    return render(request, 'stella/listing-detail.html', variables)


@csrf_exempt
def apply_subscription(request, sub_id, listing_id):
    token = request.POST['stripeToken']
    # test if user has a stripe customer record and create if not
    try:
        customer = StripeCustomer.objects.get(user=request.user)
        stripe_customer = stripe.Customer.retrieve(customer.stripe_id)
        stripe_customer.sources.create(source = token)
    except:
        stripe_customer = stripe.Customer.create(
            email=request.user.email,
            source=token,
        )
        customer = StripeCustomer (
            user = request.user,
            stripe_id = stripe_customer['id']
        )
        customer.save()
         
    # apply subscription
    stripe.Subscription.create(
        customer=customer.stripe_id,
        plan=sub_id,
    )
    
    listing = Listing.objects.get(id=listing_id)
    listing.pricelist = PriceList.objects.get(id=sub_id)
    listing.expires = listing.set_expires()
    listing.save()
    
    variables = {}
    variables['listing'] = listing
    # tell user
    return render (request, 'payment/subscription_confirmed.html', variables)

@login_required
def listing_sold(request, listing_id):
    listing = Listing.objects.get(id=listing_id)
    if listing.user != request.user: return HttpResponseForbidden()
    
    listing.sold = True
    listing.save()
    
    if listing.review: 
        form = ReviewRequestForm(initial={'username' :listing.review.user.username})
        form.fields['username'].widget.attrs['disabled'] = True
    else : form=ReviewRequestForm()
    
    if request.method == 'POST':
        form = ReviewRequestForm(request.POST)
        if form.is_valid():
            #create a review record
            review = Review(
                user = request.user,
                listing = listing,
                )
            review.save()
            
            #send it to the user
            recipient = User.objects.get(username = request.POST['username'])
            review.send_email(recipient, request)
            listing.review = review
            listing.save()
    
    variables = {}
    variables['listing'] = listing
    variables['reviewRequestForm'] = form
    return render(request, 'stella/listing-sold.html', variables)

def complete_review(request, code):
    try: review = Review.objects.get(code=code)
    except: return HttpResponseNotFound('Record Not Found')
    
    form = ReviewForm(instance = review)
    
    if request.method == 'POST':
        form = ReviewForm(request.POST, instance=review)
        form.save()
        messages.success(request, 'Thank you for your feedback')
    
    variables = {}
    variables['form'] = form
    variables['review'] = review
    variables['noCookieChoices'] = True
    return render (request, 'stella/review.html', variables)


@login_required
def create_coupon(request):
    if request.user.userprofile.coupon_eligable():
        coupon = Coupon (
            user = request.user,
            coupon_type = "Referral"
        )
        coupon.save()
        messages.success(request, "Congratulations, your friends have now helped you get a free listing!")
    else:
        messages.error(request, 'You are not eligable for a Coupon at this time')
    return redirect('goto-profile')

class RelistItem(LoginRequiredMixin, DetailView):
    model = Listing

    def get (self, *args, **kwargs):
        listing = self.get_object()
        listing.id = None
        listing.review = None
        listing.token = None
        listing.expires = None
        listing.live = False
        listing.deleted = False
        listing.featured = False
        listing.sold = False
        listing.views = 0
        listing.save()
        for watcher in listing.watchers.all(): listing.watchers.remove(watcher)
        return redirect ('listing-detail', listing.id) 