import csv
from django.http import HttpResponse

def download_institutions(modeladmin, request, queryset):
    # Create the HttpResponse object with the appropriate CSV headers.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="institutions.csv"'

    writer = csv.writer(response)

    writer.writerow(["Name", "Email"])
    for q in queryset:
        writer.writerow([q.institution,q.email])
    return response