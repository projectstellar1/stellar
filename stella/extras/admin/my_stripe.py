from stella.settings import STRIPE_SECRET_KEY
import stripe

stripe.api_key = STRIPE_SECRET_KEY

def set_up_plans(modeladmin, request, queryset):
    listings = queryset
    for l in listings:
        if not l.stripe:
            plan = stripe.Plan.create(
                name=l.name,
                id=l.id,
                interval=l.interval,
                interval_count=l.interval_count,
                currency="gbp",
                amount=int(l.price*100),
                trial_period_days = l.trial_period,
                )
            l.stripe = True
            l.save()
        else:
            plan = stripe.Plan.retrieve(str(l.id))
            plan.name = l.name
            plan.trial_period_days = l.trial_period
            plan.save()
            l.interval = plan.interval
            l.interval_count = plan.interval_count
            l.price = plan.amount/100
            l.save()