from django.shortcuts import render
from django.utils import timezone
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import operator
from django.utils import timezone
import requests
import json
from random import shuffle
from stella.settings import PAGINATOR_LENGTH
from stella.models import Listing, Category, Category2, Category3
from stella.forms import BrowseForm

VALID_SORTS = {
    'oldest' : 'id',
    'newest' : '-id',
    'cheapest' : 'price',
    'costliest' : '-price',
}

DEFAULT_SORT = 'expires'

def show_results (request, queryset, **kwargs):
    variables = {}
    
    listings_list = queryset
    
    paginator = Paginator(listings_list, PAGINATOR_LENGTH)
    page = request.GET.get('page')
    try: listings = paginator.page(page)
    except PageNotAnInteger: listings = paginator.page(1)
    except EmptyPage: listings = paginator.page(paginator.num_pages)
    
    variables['listings'] = listings
    
    try: variables['featured'] = kwargs.pop('featured')
    except: pass

    return render(request, 'stella/listing-grid.html', variables)


def search_results (request, **kwargs):
    query = Q()
    query = query & Q(deleted=False, live=True, sold=False, user__userprofile__institution = request.user.userprofile.institution)
    #query = query & Q(expires__gte=timezone.now())
    
    search_string = kwargs.pop('keywords',None)
    if search_string != None:
        keyword_query = Q()
        try: keyword_query = reduce(operator.or_, (Q(name__icontains=x) for x in search_string)) # these are the search parameters
        except: pass

        description_query = Q()
        try: description_query = reduce(operator.or_, (Q(description__icontains=x) for x in search_string)) # these are the search parameters
        except: pass
        
        query = query & (keyword_query | description_query)
    
    listing_type = kwargs.pop('listing_type', None)
    if listing_type : query = query & Q(type=listing_type)
    holiday = kwargs.pop('holiday', None)
    if holiday : query = query & Q(holiday_club=True)
    
    cid = kwargs.pop('primary_category', None)
    if cid : query = query & Q(category__id=cid)
    
    sort_key = request.GET.get('sort_key', DEFAULT_SORT)
    sort = VALID_SORTS.get(sort_key, DEFAULT_SORT)
    listings = sorted(Listing.objects.filter(query).order_by(sort), key=lambda l: -l.is_featured)
    #listings = Listing.objects.filter(query).order_by(sort)
    
    expired_time = timezone.now()
    query = query & Q(expires__gte=expired_time)
    featured = list(Listing.objects.filter(query))
    shuffle(featured)
    
    return listings, featured
    
def search_type (request, listing_type):
    if listing_type == "Holiday":
        listing_type = "Club"
        listings = search_results(request, listing_type=listing_type, holiday=True)
    else: listings = search_results(request, listing_type=listing_type)
    return show_results(request, listings)

    
def view_listing (request):
    try: search_string = request.GET['keywords'].split()
    except: search_string = None
    listings, featured = search_results (request, keywords=search_string)
    
    return show_results(request, listings, featured=featured)

def view_category (request, cid):
    listings, featured = search_results(request, primary_category=cid)
    return show_results(request, listings, featured=featured)

def view_subcategory (request, sid):
    listings, featured = search_results(request, sub_category=sid)
    return show_results(request, listings, featured=featured)

def my_listing (request, uid):
    variables = {}
    variables['listings'] = request.user.userprofile.my_listings()
    variables['sold'] = request.user.userprofile.my_sold_listings()
    
    return render(request, 'stella/my-listing.html', variables)

def advanced_search(request):
    keyword = request.GET.get('keywords', None)
    try : min_price = float(request.GET['min_price'])
    except : min_price = 0
                            
    try: max_price = float(request.GET['max_price'])
    except: max_price = 0
    
    query = Q(deleted=False, live=True, sold=False)
    keyword_query = Q()
    description_query = Q()
    price_query = Q()
    category_query = Q()
    condition_query = Q()

    try: search_string = request.GET['keywords'].split()
    except: search_string = None
    try: keyword_query = reduce(operator.or_, (Q(name__icontains=x) for x in search_string)) # these are the search parameters
    except: pass
    
    try: description_query = reduce(operator.or_, (Q(description__icontains=x) for x in search_string)) # these are the search parameters
    except: pass
    
    price_query = Q(price__gte=min_price)
    if max_price != 0: price_query = price_query & Q(price__lte=max_price)

    try:
        category = request.GET['categoryAS']
        category_query = Q(category=category)
    except:pass
    try :
        category2 = request.GET['category2AS']
        if category2 != '0': category_query = Q(category=category, category2=category2)
    except: pass
    try :
        category3 = request.GET['category3AS']
        if category3 != '0': category_query = Q(category=category, category2=category2, category3=category3)
    except : pass
    
    try:
        condition = request.GET['condition']
        if condition != "": condition_query = Q(condition=condition)
    except: pass

    query = query & (keyword_query | description_query) & price_query & category_query & condition_query
    
    sort_key = request.GET.get('sort_key', DEFAULT_SORT)
    sort = VALID_SORTS.get(sort_key, DEFAULT_SORT)
    listings = sorted(Listing.objects.filter(query).order_by(sort), key=lambda l: -l.is_featured)
    
    expired_time = timezone.now()
    query = query & Q(expires__gte=expired_time)
    featured = list(Listing.objects.filter(query))
    shuffle(featured)
            
    return show_results(request, listings, featured=featured)

def watch_list(request):
    variables = {}
    variables['listings'] = request.user.userprofile.watch_list()
    return render(request, 'stella/watch-list.html', variables)




def browse(request, category_id, category2_id, category3_id):    
    variables = {}
    query = Q()
    query = query & Q(deleted=False, live=True, sold=False, user__userprofile__institution = request.user.userprofile.institution)
    
    if category3_id != '0'   : query = query & Q(category3__id=category3_id)
    elif category2_id != '0' :
        query = query & Q(category2__id=category2_id)
    elif category_id != '0'  :
        query = query & Q(category__id=category_id)
    
    if category2_id != '0': variables['cat3'] = Category3.objects.filter(parent=Category2.objects.get(id=int(category2_id)))
    if category_id != '0': variables['cat2'] = Category2.objects.filter(parent=Category.objects.get(id=int(category_id)))
    variables['categories'] = Category.objects.all()

    variables['cat1_id'] = int(category_id)
    variables['cat2_id'] = int(category2_id)
    variables['cat3_id'] = int(category3_id)
    
    sort_key = request.GET.get('sort_key', DEFAULT_SORT)
    sort = VALID_SORTS.get(sort_key, DEFAULT_SORT)
    listings_list = sorted(Listing.objects.filter(query).order_by(sort), key=lambda l: -l.is_featured)
    
    paginator = Paginator(listings_list, PAGINATOR_LENGTH)
    page = request.GET.get('page')
    try: listings = paginator.page(page)
    except PageNotAnInteger: listings = paginator.page(1)
    except EmptyPage: listings = paginator.page(paginator.num_pages)
    
    variables['listings'] = listings
    
    expired_time = timezone.now()
    query = query & Q(expires__gte=expired_time)
    featured = list(Listing.objects.filter(query))
    shuffle(featured)
        
    variables['featured'] = featured
    
    return render(request, 'stella/browse-grid.html', variables)