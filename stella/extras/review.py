from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect

from stella.models import Listing, Review
from stella.forms import ReviewForm

def add_review(request, listing_id):
    listing = Listing.objects.get(id=listing_id)
    if request.method == 'POST':
        form = ReviewForm(request.POST, instance=listing)
        if form.is_valid():
            review = form.save()
            listing.reviews.add(review)
            listing.save()
            messages.success(request, 'Review successfully added.')
            
    
    variables = {}
    variables['listing'] = listing
    variables['review_form'] = form
    return render(request, 'stella/listing-detail.html', variables)