from django.shortcuts import render
from django.core.mail import EmailMessage
from django.contrib import messages

from stella import settings
from stella.forms import ContactForm, ReviewForm
from stella.models import Listing

def contact_us(request, to_email):
    form = ContactForm()
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            email = EmailMessage("Contact Form: " + request.POST.get("subject"),
                                 request.POST.get("name") + " with email " + request.POST.get("email") + " sent the following: " + request.POST.get("message"),
                                 to=[to_email])
            email.send()
            email_sent = True
            messages.success(request, 'Contact form sent successfully.')
    
    variables = {}
    variables['form'] = form
    variables['maps_key'] = settings.GOOGLE_MAPS_KEY
    return render(request, 'stella/contact.html', variables)

def contact_them (request, listing_id):
    listing = Listing.objects.get(id=listing_id)
    form = ContactForm()
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            email = EmailMessage("Contact Form: " + request.POST.get("subject"),
                                 request.POST.get("name") + " with email " + request.POST.get("email") + " sent the following: " + request.POST.get("message"),
                                 to=[listing.email])
            email.send()
            email_sent = True
            messages.success(request, 'Contact form sent successfully.')
    
    variables = {}
    variables['contact_form'] = form
    variables['review_form'] = ReviewForm()
    variables['listing'] = listing
    return render(request, 'stella/listing-detail.html', variables)