from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.shortcuts import reverse
from django.contrib import messages
import datetime
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from ckeditor.fields import RichTextField
import math
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Q

from random import randint
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.template import Context
from django.contrib.sites.shortcuts import get_current_site

from django.db.models import Avg
from djstripe.models import Coupon

from local_messages.models import Message

from parameters import CONDITION_OPTIONS


def pkgen():
    ''' Creates a random code to for the review
    '''
    from base64 import b32encode
    from hashlib import sha1
    from random import random
    rude = ('lol',)
    bad_pk = True
    while bad_pk:
        pk = b32encode(sha1(str(random())).digest()).lower()[:30]
        bad_pk = False
        for rw in rude:
            if pk.find(rw) >= 0: bad_pk = True
    return pk

def referral_code():
    import string, random
    pk = ''.join([random.choice(string.ascii_uppercase+string.digits) for i in range(3)])
    pk += ''.join([random.choice(string.ascii_uppercase) for i in range(2)])
    pk += ''.join([random.choice(string.digits) for i in range(1)])
    return (pk)


class NewInstitution(models.Model):
    institution = models.CharField(max_length=200)
    email = models.EmailField()
    
    def __unicode__(self):
        return u'%s' % (self.institution)
    
class Institution(models.Model):
    name = models.CharField(
        max_length=200,
        verbose_name = 'Institution')
    
    def __unicode__(self):
        return u'%s' % (self.name)
    
class ValidEmail(models.Model):
    extension = models.CharField(max_length=200)
    institution = models.ForeignKey(Institution)
    
    def __unicode__(self):
         return u'%s %s' % (self.institution, self.extension)

class UserProfile(models.Model):
    user=models.OneToOneField(User, on_delete=models.CASCADE)
    institution=models.ForeignKey(
        Institution,
        null=True,
        blank=True)
    telephone = models.CharField(
        verbose_name="Telephone",
        max_length = 12,
        null=True,
        blank=True
    )
    avatar = models.ImageField(
        upload_to='avatars/',
        null=True,
        blank=True
    )
    email_notification = models.BooleanField (
        verbose_name = 'Receive Email Notification',
        default = True
    )
    verified = models.BooleanField (
        verbose_name = 'Verified User',
        default = False,
    )
    referral_code = models.CharField(
        verbose_name = 'Referral Code',
        max_length = 6,
        default = referral_code
    )
    referred_code = models.CharField(
        max_length = 6,
        blank=True,
        null = True
    )
    stripe_id = models.CharField(
        max_length = 100,
        blank=True,
        null=True
    )

    def __unicode__(self):
        try: return u'%s' % (self.user)
        except: return 'None'
    
    def school_listings (self):
        return Listing.objects.filter(user__userprofile__institution = self.institution, live=True, deleted=False, sold=False).order_by('-id')[:10]
    
    def watch_list (self):
        return Listing.objects.filter(watchers__id=self.user.id, live=True, deleted=False, sold=False)
    
    def my_listings (self):
        return Listing.objects.filter(user=self.user, deleted=False).order_by('-id')

    def my_sold_listings (self):
        return Listing.objects.filter(user=self.user, deleted=False, sold=True).order_by('-id')

    def featured (self):
        return Listing.objects.filter(user__userprofile__institution = self.institution, live=True, deleted=False, expires__gte = timezone.now(), sold=False)
    
    def response_time (self):
        myMessages = Message.objects.filter(recipient=self.user, replied_at__gte='2000-01-01')
        if len(myMessages) == 0: return 0
        total_time = datetime.timedelta(0)
        for m in myMessages:
            delta = m.replied_at-m.sent_at
            total_time += delta
        return total_time/len(myMessages)

    def rating(self):
        reviews = Review.objects.filter(user=self.user)
        if len(reviews) == 0: return 0
        total_score = 0
        count = 0
        for r in reviews:
            if r.rating :
                total_score = total_score + r.rating
                count += 1
        if count > 0 : return float(float(total_score) / float(count))
        return 0
    
    def reviews(self):
        return Review.objects.filter(user=self.user)

    def referral_url(self):
        return reverse('registration_register') + '?referral=' + self.referral_code 

    def total_referrals(self):
        return UserProfile.objects.filter(referred_code=self.referral_code, verified=True).count()
    
    def total_coupons (self):
        from coupons.models import Coupon
        return Coupon.objects.filter(user=self.user, coupon_type="Referral").count()

    def coupon_eligable(self):
        if int(self.total_referrals() / 5) > self.total_coupons() and self.total_coupons() < 5: return True
        return False

    def unused_coupon(self):
        from coupons.models import Coupon
        coupons = Coupon.objects.filter(user=self.user, dt_used__isnull=True).count()
        if coupons > 0: return True
        return False


class Category (models.Model):
    name = models.CharField (
        verbose_name = 'Top Level Category',
        max_length = 30,
    )
    photo = models.ImageField (
        upload_to = 'categories',
        blank = True,
        null=True
    )
    menu = models.BooleanField(
        default = False,
        help_text="First 5 will show in top menu"
    )
    feature = models.BooleanField(
        default = False,
        help_text = "The first three found will be featured on the home page"
    )

    def __unicode__(self): 
        return self.name 

class Category2 (models.Model):
    name = models.CharField(
        verbose_name = 'Mid Level Category',
        max_length = 30,
    )
    parent = models.ForeignKey(
        Category,
        verbose_name = 'Top Level Category'
    )
    def __unicode__(self): 
        return u'%s | %s ' % (self.parent, self.name)

class Category3 (models.Model):
    name = models.CharField(
        verbose_name = 'Low Level Category',
        max_length = 30,
    )
    parent = models.ForeignKey(
        Category2,
        verbose_name = 'Mid Level Category'
    )
    def __unicode__(self): 
        return u'%s | %s ' % (self.parent, self.name)
    
class Amenity (models.Model):
    name = models.CharField (
        verbose_name = 'Amenity',
        max_length = 30
    )
    
    def __unicode__(self):
        return u'%s' % (self.name)
    
class Review (models.Model):
    user = models.ForeignKey(
        User,
    )
    feedback = RichTextField(
        config_name='very_basic_ckeditor',
        blank = True,
        null = True
    )

    RATING_OPTIONS = (
        (1, 1),        
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5)
    )
    rating = models.IntegerField(
        verbose_name = "Stars",
        choices = RATING_OPTIONS,
        blank = True,
        null = True
    )
    code = models.CharField (
        max_length = 30,
        default=pkgen
    )
    
    def send_email(self, recipient, request):
        email_body_template = 'email/request_review.txt'
        email_subject_template = 'email/request_review_subject.txt'
        context = Context()
        
        context = {}
        context['review'] = self
        context['site'] = get_current_site(request)

        subject = render_to_string(email_subject_template, context)
        # Force subject to a single line to avoid header-injection
        # issues.
        subject = ''.join(subject.splitlines())
        message = render_to_string(email_body_template,
                                   context)
        email = EmailMessage(subject,
                             message,
                             to=[recipient.email])
        email.send()
        messages.success(request, 'Email request sent.')

class Listing(models.Model):
    user = models.ForeignKey(
        User,
    )
    name = models.CharField(
        verbose_name = 'Listing Title',
        max_length   = 45,
    )
    category = models.ForeignKey(
        Category,
        verbose_name = 'Top Level Category',
    )
    category2 = models.ForeignKey(
        Category2,
        verbose_name = 'Mid-Level Category',
        default = None,
        blank=True,
        null = True
        )
    category3 = models.ForeignKey(
        Category3,
        verbose_name = 'Low-Level Category',
        default = None,
        blank=True,
        null = True
        )
    
    description = RichTextField()
    
    image = models.ImageField(
        verbose_name = 'Image',
        blank = True,
        null = True,
    )
    price = models.FloatField (
        verbose_name = "Price",
        default = 0
    )
    amenities = models.ManyToManyField(
        Amenity,
        verbose_name = 'Amenities',
        blank = True,
    )
    token = models.CharField(
        max_length=255,
        blank = True,
        null = True,
    )
    loaded = models.DateTimeField(
        default = timezone.now,
        verbose_name="Date / Time loaded"
    )
    expires = models.DateTimeField (
        verbose_name = 'Featured Expires datetime',
        default = timezone.now,
        null=True,
        blank = True
    )
    tandc = models.BooleanField (
        verbose_name = 'Terms and Conditions',
        default = False,
    )
    live = models.BooleanField (
        verbose_name = 'Advert is Live',
        default = False
    )
    condition = models.CharField(
        verbose_name = "Condition",
        choices = CONDITION_OPTIONS,
        max_length = 30,
        default="Average"
    )
    phone_contact = models.BooleanField (
        verbose_name = 'Contact by Phone',
        default = False
    )
    email_contact = models.BooleanField (
        verbose_name = 'Contact by Email',
        default = False
    )
    review = models.ForeignKey(
        Review,
        blank=True,
        null=True
        )
    views = models.IntegerField(default=0)
    watchers = models.ManyToManyField(
        User,
        related_name = 'Watcher',
        blank=True)
    deleted = models.BooleanField(
        default = False
    )
    featured = models.BooleanField (
        default=False
    )
    sold = models.BooleanField(
        default = False
    )
    
    def __unicode__(self):
        return u'%s' % (self.name)
    
    def set_deleted(self):
        self.deleted = True
        self.live = False
        self.save()
        
    def avg_review(self):
        result = self.review.aggregate(Avg('rating')).get('rating__avg')
        if not 0 < result <= 5: result = 0
        return result
    
    def set_expires(self):
        if not (self.expires > timezone.now()): self.expires=timezone.now()
        self.expires = self.expires + datetime.timedelta(days=1)
        self.save()

    def distance(self, lng, lat):
        print math.sqrt(math.pow(self.longitude-lng,2) + math.pow(self.latitude-lat,2))
        return math.sqrt(math.pow(self.longitude-lng,2) + math.pow(self.latitude-lat,2))
    
    def update_admin(self):
        email_body_template = 'stella/admin/listing_email.txt'
        email_subject_template = 'stella/admin/listing_email_subject.txt'
        context = Context()
        
        context = {}
        context['listing'] = self

        subject = render_to_string(email_subject_template,
                           context)
        # Force subject to a single line to avoid header-injection
        # issues.
        subject = ''.join(subject.splitlines())
        message = render_to_string(email_body_template,
                                   context)
        email = EmailMessage(subject,
                             message,
                             to=[settings.DEFAULT_TO_EMAIL])
        email.send()
        
    def increment_views(self):
        self.views += 1;
        self.save()

    @property
    def is_featured(self):
        try:
            if self.expires > timezone.now(): return True
        except : pass
        return False

class ListingImages (models.Model):
    ''' Images of items for sale
    '''
    listing = models.ForeignKey(
        Listing
    )
    image = models.ImageField (
        verbose_name = "Image",
        upload_to = "listing_images",
    )
   
from local_messages.models import Message
class MessageExtension (models.Model):
    ''' Extends Message to include Listing link '''
    message=models.OneToOneField(Message, on_delete=models.CASCADE)
    listing=models.ForeignKey(
        Listing,
        null=True,
        blank=True
    )
    
    
class DirtyName (models.Model):
    ''' This class is a list of text strings which cannot be included in the user's name '''
    name = models.CharField(
        max_length = 30,
        verbose_name = "Banned strings"
    )
    
    def __unicode__(self):
        return u'%s' % (self.name)
    
class Help (models.Model):
    ''' This class populates the Help page '''
    question = models.TextField (
        verbose_name = 'Question'
    )
    answer = models.TextField (
        verbose_name = 'Answer'
    )
    order = models.IntegerField(
        default = 999
    )
    
    def __unicode__(self):
        return u'%s' % (self.question)
    
class SafeExchange (models.Model):
    ''' This populates the Safe Exchange page '''
    text = RichTextField (
        verbose_name = 'Text'
    )
    order = models.IntegerField(
        default = 999
    )
    
    def __unicode__(self):
        return u'%s' % (self.order)
    
    
    
class ReportListing (models.Model):
    user = models.ForeignKey(User)
    listing = models.ForeignKey(Listing)
    
    def __unicode__(self):
        return u'%s' % (self.listing.name)