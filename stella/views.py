import requests
import json

from django.contrib.auth.models import User
from django.shortcuts import render
from models import Listing, Category, Category2, Category3, UserProfile, ListingImages, Help, ReportListing, SafeExchange
from forms import ReviewForm, ContactForm, UserRegForm, InstitutionForm, UserForm, UserProfileForm
from django.utils import timezone

from django.contrib.auth.decorators import login_required
from django.contrib import messages
from extras.search import search_results
from django.http import HttpResponseForbidden, HttpResponse, HttpResponseRedirect
from django.contrib.auth.forms import PasswordChangeForm

from registration.backends.model_activation.views import RegistrationView
from registration.models import RegistrationProfile
from registration import signals
from django.contrib.sites.shortcuts import get_current_site
from django.core import signing
from django.template.loader import render_to_string
from django.conf import settings
from django.shortcuts import redirect

from django.http import HttpResponseForbidden
import access_permissions

REGISTRATION_SALT = getattr(settings, 'REGISTRATION_SALT', 'registration')

class MyRegistrationView (RegistrationView):
    form_class = UserRegForm
    email_body_template = 'registration/activation_email.txt'
    email_subject_template = 'registration/activation_email_subject.txt'
    
    def get_context_data(self, *args, **kwargs):
        ctx = super(MyRegistrationView, self).get_context_data(*args, **kwargs)
        ctx['referral_code'] = self.request.GET.get('referral', False)

        return ctx

    def register(self, form):
        new_user = self.create_inactive_user(form)
        signals.user_registered.send(sender=self.__class__,
                                     user=new_user,
                                     request=self.request)
        
        referred_code = form.cleaned_data['referral_code']
        if len(referred_code) > 0 :
            new_user.userprofile.referred_code = referred_code
            new_user.userprofile.save()
        return new_user

    def get_success_url(self, user):
        return ('registration_complete', (), {})
        
    def create_inactive_user(self, form):
        """
        Create the inactive user account and send an email containing
        activation instructions.

        """
        new_user = form.save(commit=False)
        new_user.is_active = False
        new_user.save()

        self.send_activation_email(new_user)

        return new_user

    def get_activation_key(self, user):
        """
        Generate the activation key which will be emailed to the user.

        """
        return signing.dumps(
            obj=getattr(user, user.USERNAME_FIELD),
            salt=REGISTRATION_SALT
        )

    def get_email_context(self, activation_key):
        """
        Build the template context used for the activation email.

        """
        return {
            'activation_key': activation_key,
            'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
            'site': get_current_site(self.request)
        }

    def send_activation_email(self, user):
        """
        Send the activation email. The activation key is simply the
        username, signed using TimestampSigner.

        """
        activation_key = self.get_activation_key(user)
        context = self.get_email_context(activation_key)
        context.update({
            'user': user
        })
        subject = render_to_string(self.email_subject_template,
                                   context)
        # Force subject to a single line to avoid header-injection
        # issues.
        subject = ''.join(subject.splitlines())
        message = render_to_string(self.email_body_template,
                                   context)
        user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)


    
def index(request, **kwargs): 
    if request.user.is_authenticated(): return home(request)
    
    return redirect('registration_register')

def home(request):
    if not request.user.is_authenticated(): return index(request)
    
    if (len(request.user.first_name) < 1) or (len(request.user.last_name) < 1) : return redirect('goto-profile')
    
    variables = {} 
    variables['listings'] = search_results(request)
    variables['categories'] = Category.objects.all()
    
    return render(request, 'stella/home.html', variables)

def help(request):
    variables = {}
    variables['help'] = Help.objects.all().order_by('order')
    return render(request, 'stella/help.html', variables)

def no_subscribe(request):
    request.session['subscribed'] = True
    return home(request)

def no_institution (request):
    variables = {}
    form = InstitutionForm()
    
    if request.method == 'POST':
        form = InstitutionForm(request.POST)
        if form.is_valid ():
            form.save()
            messages.success(request, "Thank you.  We've noted your school and we'll include it when we have enough interest.")
            
    variables['form'] = form
    return render(request, 'stella/no-institution.html', variables)

def detail_listing(request, listing_id):
    variables = {}
    listing = variables['listing'] = Listing.objects.get(id=listing_id)
    
    if not access_permissions.can_access_listing(request.user, listing): return HttpResponseForbidden()
    
    listing.increment_views()
    
    if request.user.is_authenticated: form_data = {'user' : request.user}
    else: form_data = {}
    
    variables['review_form'] = ReviewForm(initial=form_data)
    variables['contact_form'] = ContactForm()
    return render(request, 'stella/listing-detail.html', variables)

def pricelist(request):
    variables = {}
    variables['pricelist'] = PriceList.objects.all().order_by('priority')
    return render(request, 'stella/pricelist.html', variables)

def terms_conditions(request):
    return render(request, 'stella/terms-conditions.html')

def safe_exchange(request):
    variables = {}
    variables['text'] = SafeExchange.objects.all().order_by('order')
    return render(request, 'stella/safe-exchange.html', variables)

@login_required
def profile(request, **kwargs):
    variables = {}
    
    form = UserProfileForm(instance=UserProfile.objects.get(user=request.user))
    userForm = UserForm(instance=request.user)
    
    if request.method == 'POST':
        form = UserProfileForm(request.POST, request.FILES, instance=UserProfile.objects.get(user=request.user))
        userForm = UserForm(request.POST, instance=request.user)
        if form.is_valid() and userForm.is_valid():
            userForm.save()
            form.save()
            messages.success(request, 'Profile Updated')
            
    variables['form'] = form
    variables['userForm'] = userForm
    variables['passwordForm'] = PasswordChangeForm(request.user)
    variables['stripe_key'] = settings.STRIPE_PUBLISH_KEY
    return render(request, 'stella/profile.html', variables)

@login_required
def change_password(request):
    variables = {}
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            variables['form'] = CustomerForm(instance=request.user.customer)
            variables['userForm'] = UserForm(instance=request.user)
            variables['passwordForm'] = PasswordChangeForm(request.user)
            return render(request, 'abysm/profile.html', variables)
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/password_change_form.html', {'form': form})

@login_required
def amend_watcher(request, listing_id):
    ''' Toggles watch flag 
    '''
    user=request.user
    listing = Listing.objects.get(id=listing_id)
    if not access_permissions.can_access_listing(request.user, listing) : return HttpResponseForbidden
    
    if user in listing.watchers.all():
        listing.watchers.remove(user)
        messages.success(request,'Watch removed')
    else:
        listing.watchers.add(user)
        messages.success(request,'Watch added')
    
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@login_required
def stop_watching_selected(request):
    ''' removes the selected items from the watch list
    '''
    if request.POST:
        listings = [v for k, v in request.POST.items() if k.startswith('featured')]
        for listing in listings:
            l = Listing.objects.get(id=int(listing))
            l.watchers.remove(request.user)
        
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@login_required
def delete_selected(request):
    ''' gives the selected items a deleted status
    '''
    if request.POST:
        listings = [v for k, v in request.POST.items() if k.startswith('featured')]
        for listing in listings:
            l = Listing.objects.get(id=int(listing))
            l.set_deleted()
        
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))        

@login_required
def ajax_delete_image(request):
    ''' Deletes image from item
    '''
    info = request.GET.getlist('info[]')
    response_data = {}
    
    try:
        image_id = info[0].replace('drag','')
        image = ListingImages.objects.get(id=int(image_id))
        image.delete()
        response_data['ready'] = 'True'
    except: pass
    
    return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

@login_required
def ajax_change_category(request):
    '''
    Changes selected category
    Passes back sub-categories
    '''
    response_data = {}

    info = request.GET.getlist('info[]')
    try: i = int(info[0])
    except: info[0]=0
    
    # if this is top level we cannot pass up keys
    if int(info[0]) == 0:
        response_data['category'] = 'None'
        response_data['category2'] = 'None'
        response_data['mid_categories_boolean'] = 'False'
        response_data['category3'] = 'None'
    else:
        category = Category.objects.get(id=int(info[0]))
        xxx = response_data['mid_categories'] = list(Category2.objects.filter(parent=category).values('id','name'))
        if len(xxx) > 0: response_data['mid_categories_boolean'] = 'True'
        else: response_data['mid_categories_boolean'] = 'False'
        
        response_data['category'] = category.name
        response_data['category3'] = 'None'

    response_data['ready'] = 'True'
    
    return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

@login_required
def ajax_change_category2(request):
    '''
    Changes selected category
    Passes back low-categories
    '''
    response_data = {}

    info = request.GET.getlist('info[]')
    
    # if this is top level we cannot pass up keys
    print info[0]
    if int(info[0]) == 0:
        response_data['category'] = 'None'
        response_data['category2'] = 'None'
        response_data['low_categories_boolean'] = 'False'
        response_data['category3'] = 'None'
    else:
        category = Category2.objects.get(id=int(info[0]))
        print category
        xxx = response_data['low_categories'] = list(Category3.objects.filter(parent=category).values('id','name'))
        if len(xxx) > 0: response_data['low_categories_boolean'] = 'True'
        else: response_data['low_categories_boolean'] = 'False'
        print "OK"
        response_data['category'] = category.name

    response_data['ready'] = 'True'
    
    return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

@login_required
def ajax_flag_item(request):
    '''
    Creates a record of the reported item
    '''
    response_data = {}

    info = request.GET.getlist('info[]')
    
    listing = Listing.objects.get(id=int(info[0]))
    user = User.objects.get(id=int(info[1]))
    
    try:
        report = ReportListing.objects.get(user=user, listing=listing)
        response_data['message'] = 'Report sent previously'
    except:
        report = ReportListing(
            user=user,
            listing=listing
        )
        report.save()
        response_data['message'] = "Listing reported"

    response_data['ready'] = 'True'
    
    return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )


from local_messages.views import compose

from forms import MyComposeForm
@login_required
def direct_message (request, user_id, listing_id):
    return compose (request, recipient=User.objects.get(id=user_id).username, subject=Listing.objects.get(id=listing_id).name, form_class=MyComposeForm)