from .models import Category

def featured_categories(request):
    return {
        'menu_category' : Category.objects.filter(menu=True)[:5],
        'featured_category' : Category.objects.filter(feature=True)[:3]
    }