//  selects and checks all checkboxes with a name starting with indexName
//  eg selectAll('featured') will select all checkboxes whose names starts with 'featured'


function selectAll(indexName) {
  var els = []
  var checkboxes = document.getElementsByTagName("input");
  for (var i=0; i<checkboxes.length; i++) {
    if (checkboxes[i].name.indexOf(indexName) == 0) {
        checkboxes[i].checked = true ;
        }
    }
}