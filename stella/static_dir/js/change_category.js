// Changes the Category options based on current selections
// catId if the category field being changed in case there are multiple forms available


function appendOption (appendInfo,catId) {
    var e = document.getElementById(catId);
    var option = document.createElement("option");
    option.innerHTML = "  &#8627;" + appendInfo.name;
    option.value = appendInfo.id;
    e.appendChild(option);    
}

function changeCategory2(secId) {
    var id2 = "id_category2"+secId;
    var id3 = "id_category3"+secId;
    var e2 = document.getElementById(id2);
    var e3 = document.getElementById(id3);
    var length = e2.options.length;
    var category_id = 0;
    if (e2.selectedIndex == -1) {
        category_id = 0;
    } else {
        category_id = e2.options[e2.selectedIndex].value;
    }
    var info = [];
    info[0] = category_id;
    
    $.ajax({
        url : "/ajax_change_category2/", // the endpoint
        type : "GET", // http method
        data : { info : info }, // data sent with the post request
        // handle a successful response
        success : function(json) {
            if (json.ready == 'True') {
                console.log(json);
                // remove current options
                for (i = 0; i < length+1; i++) {
                    e3.options[0] = null;
                }
                // hide unwanted menu items
                if (json.low_categories_boolean == 'False') {
                    e3.style.display = 'none';
                } else {
                    e3.style.display = 'block';
                    option = document.createElement("option");
                    option.innerHTML = "Select option";
                    option.value = 0;
                    option.selected = true;
                    e3.appendChild(option);    
                    for (var i = 0; i < json.low_categories.length; i++) {
                        appendOption(json.low_categories[i],id3);
                    }    
                }
            }
        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            toastr.warning("Something went wrong. Please refresh screen and try again.\n" + xhr + errmsg + err );
        }
    });
}

function changeCategory(secId) {
    var id1 = "id_category"+secId;
    console.log(secId, " ", id1);
    var id2 = "id_category2"+secId;
    var id3 = "id_category3"+secId;
    var e =  document.getElementById(id1);
    var e2 = document.getElementById(id2);
    var e3 = document.getElementById(id3);
    var length = e.options.length;
    var category_id = 0;
    if (e.selectedIndex == -1) {
        category_id = 0;
    } else {
        category_id = e.options[e.selectedIndex].value;
    }
    var info = [];
    info[0] = category_id;
    $.ajax({
        url : "/ajax_change_category/", // the endpoint
        type : "GET", // http method
        data : { info : info }, // data sent with the post request
        // handle a successful response
        success : function(json) {
            if (json.ready == 'True') {
                console.log(json);
                // remove current options
                for (i = 0; i < length+1; i++) {
                    e2.options[0] = null;
                }
                for (i = 0; i < length+1; i++) {
                    e3.options[0] = null;
                }
                
                // hide unwanted menu items
                if (json.mid_categories_boolean == 'False') {
                    e2.style.display = 'none';
                } else {
                    e2.style.display = 'block';
                    option = document.createElement("option");
                    option.innerHTML = "Select option";
                    option.value = 0;
                    option.selected = true;
                    e2.appendChild(option);    
                    for (var i = 0; i < json.mid_categories.length; i++) {
                        appendOption(json.mid_categories[i],id2);
                    }    
                }
                
                if (json.category3 == 'None') {
                    e3.style.display = 'none';
                }
              
            }
        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            toastr.warning("Something went wrong. Please refresh screen and try again.\n" + xhr + errmsg + err );
        }
    });
}

// This function gets cookie with a given name
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

/*
The functions below will create a header with csrftoken
*/

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});