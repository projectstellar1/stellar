from __future__ import division

from django import template
from stella.settings import STRIPE_CURRENCY, STRIPE_PUBLISH_KEY

register = template.Library()

@register.filter(name='pence')
def pence(x):
    return x*100

@register.simple_tag(name="stripe_key")
def stripe_key():
    return STRIPE_PUBLISH_KEY

@register.simple_tag(name="stripe_currency")
def stripe_currency():
    return STRIPE_CURRENCY