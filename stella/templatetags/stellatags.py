from django import template
from django.utils import timezone
import datetime

from stella.models import Institution, Listing, Category
register = template.Library()

@register.filter(name='in_past')
def in_past(myDate):
    now = timezone.now()
    today = datetime.date(now.year, now.month, now.day)
    
    if myDate < today: return True
    else: return False

@register.filter(name='days_listed')
def days_listed (listing_id):
    listing = Listing.objects.get(id=int(listing_id))
    d0 = timezone.now()
    d1 = listing.loaded
    delta = d0-d1
    return delta.days

@register.filter(name='hours_listed')
def hours_listed (listing_id):
    listing = Listing.objects.get(id=int(listing_id))
    d0 = timezone.now()
    d1 = listing.loaded
    delta = d0-d1
    days, seconds = delta.days, delta.seconds
    
    return days * 24 + seconds // 3600

@register.filter(name='minutes_listed')
def minutes_listed (listing_id):
    listing = Listing.objects.get(id=int(listing_id))
    d0 = timezone.now()
    d1 = listing.loaded
    delta = d0-d1
    days, seconds = delta.days, delta.seconds
    return (seconds % 3600) // 60


@register.filter(name='response_time')
def response_time(responseSeconds):
    if responseSeconds < 12*60*60 : return 'Very Quick'
    if responseSeconds < 24*60*60 : return 'Quick'
    if responseSeconds < 48*60*60 : return 'Average'
    if responseSeconds < 72*60*60 : return 'Slow'
    return 'Very Slow'

from django.utils import timezone
@register.filter(name='is_featured')
def is_featured(listing):
    if listing.expires > timezone.now(): return True
    else: return False
    
@register.inclusion_tag('partials/institution-list.html')
def institution_list():
    return {'institution' : Institution.objects.all().order_by('name')}

@register.inclusion_tag('partials/select-categories.html')
def root_categories():
    return {'categories' : Category.objects.all().order_by('name')}

from stella.forms import AdvancedSearchForm
@register.inclusion_tag('partials/advanced-search.html')
def advanced_search():
    return {'advancedSearchForm' : AdvancedSearchForm() }

from stella.forms import LeftSearchForm
@register.inclusion_tag('partials/left-search-menu.html')
def left_menu_search():
    return {'form' : LeftSearchForm() }
