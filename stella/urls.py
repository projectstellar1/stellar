"""klair URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import password_reset

from extras.registration_activation_resend import resend_activation_email
from extras.contact_us import contact_us, contact_them
from extras.add_listing import add_listing, process_payment, buy_subscription, apply_subscription, make_live, listing_sold, \
    complete_review, create_coupon, RelistItem
from extras.review import add_review
from extras.search import view_listing, view_category, view_subcategory, my_listing, search_type, advanced_search, watch_list, \
    browse

from extras.user_profiles import AddCreditCard

import views

admin.autodiscover()
admin.site.site_header = 'Stella Admin'

from registration.backends.model_activation.views import RegistrationView
from django.views.generic.base import TemplateView
from forms import UserRegForm
import regbackend

#django-registration and admin
urlpatterns = [
    url(r'^admin/', admin.site.urls, name='admin'),
    url(r'^accounts/register/$', views.MyRegistrationView.as_view(form_class=UserRegForm), name='registration_register'),
    
    url(r'^password_reset/$', password_reset, {'email_template_name': 'registration/password_reset_email.html'}, name="password_reset"),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    url(r'^accounts/profile/$', views.home, name='profile'),
    url(r'^profile/$', views.profile, name='goto-profile'),
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^register/activation_resend/$', resend_activation_email, name='activation-resend'),
    url(r'^password/$', views.change_password, name='change_password'),
    
    url(r'^help/$', views.help, name='help'),
]

#ckeditor
'''urlpatterns += [
    (r'^ckeditor/', include('ckeditor_uploader.urls')),
]
'''

#main menu items
urlpatterns += [
    url(r'^$', views.index, name='index'),
    url(r'^home/$', views.home, name='home'),
    url(r'^no_institution/$', views.no_institution, name="no-institution"),
    url(r'^contact/', contact_us, {'to_email': settings.DEFAULT_TO_EMAIL}, name='contact-us'),
    url(r'^contact_them/(?P<listing_id>[0-9]+)/$', contact_them, name="contact-them"),
    url(r'^pricelist/$', views.pricelist, name='pricelist'),
    url(r'^no_subscribe/$', views.no_subscribe, name='no-subscribe'),
    url(r'^terms_conditions/$', views.terms_conditions, name='terms-conditions'),
    url(r'^safe_exchange/$', views.safe_exchange, name='safe-exchange'),
]

# add listings and subscriptions
urlpatterns += [
    url(r'^add_listing/$', add_listing, name='add-listing'),
    url(r'^edit_listing/(?P<listing_id>[0-9]+)/$', add_listing, name='listing-edit'),
    url(r'^process_payment/(?P<listing_id>[0-9]+)/$', process_payment, name='process-payment'),
    url(r'^buy_subscription/(?P<listing_id>[0-9]+)/$', buy_subscription, name='buy-subscription'),
    url(r'^detail_listing/(?P<listing_id>[0-9]+)/$', views.detail_listing, name='listing-detail'),
    url(r'^apply_subscription/(?P<sub_id>[0-9]+)/(?P<listing_id>[0-9]+)/$', apply_subscription, name='apply-subscription'),
    url(r'^make_live/(?P<listing_id>[0-9]+)/$', make_live, name="make-live"),
]

# reviews
urlpatterns += [
    url(r'^add_review/(?P<listing_id>[0-9]+)/$', add_review, name='add-review'),
    url(r'^complete_review/(?P<code>[0-9a-zA-Z]+)/$', complete_review, name='complete_review'),
]

from forms import MyComposeForm, MyReplyForm
from local_messages.views import compose, reply
# messages
urlpatterns += [
    url(r'^direct_message/(?P<user_id>[0-9]+)/(?P<listing_id>[0-9]+)/$', views.direct_message, name='direct-message'),
    url(r'^messages/compose/', compose, {'form_class': MyComposeForm,}, name="messages_compose"),
    url(r'^messages/reply/(?P<message_id>[\d]+)/$', reply, {'form_class': MyReplyForm,}, name="messages_reply"),
    url(r'^messages/', include('local_messages.urls')),
]

#search / view listings
urlpatterns += [
    url(r'^view_listing/$', view_listing, name="view-listing"),
    url(r'^view_category/(?P<cid>[0-9]+)/$', view_category, name="view-category"),
    url(r'^view_subcategory/(?P<sid>[0-9]+)/$', view_subcategory, name="view-subcategory"),
    url(r'^my_listing/(?P<uid>[0-9]+)/$', my_listing, name="my-listing"),
    url(r'^view_listing_type/(?P<listing_type>[A-Za-z]+)/$', search_type, name="search-type" ),
    url(r'^advanced_search/$', advanced_search, name='advanced-search'),
    url(r'^watch_list/$', watch_list, name="watch-list"),
    url(r'^amend_watcher/(?P<listing_id>[0-9]+)/$', views.amend_watcher, name="amend-watch"),
    url(r'^stop_watching_selected/$', views.stop_watching_selected, name="stop-watching"),
    url(r'^delete_selected/$', views.delete_selected, name="delete-selected"),
    url(r'^listing_sold/(?P<listing_id>[0-9]+)/$', listing_sold, name="listing-sold"),
    url(r'^browse/(?P<category_id>[0-9]+)/(?P<category2_id>[0-9]+)/(?P<category3_id>[0-9]+)/$', browse, name="browse"),
]

# ajax
urlpatterns += [
    url(r'^ajax_delete_image/$', views.ajax_delete_image),
    url(r'^ajax_change_category/$', views.ajax_change_category),
    url(r'^ajax_change_category2/$', views.ajax_change_category2),
    url(r'^ajax_flag_item/$', views.ajax_flag_item),
]

urlpatterns += [
    url(r'^payment-processing/', include('djstripe.urls', namespace='djstripe')),
    url(r'^create-coupon/', create_coupon, name="create-coupon"),
    url(r'^add-payment-card/(?P<pk>[0-9]+)/$', AddCreditCard.as_view(), name="add-payment-details"),
    url(r'^relist/(?P<pk>[0-9]+)/$', RelistItem.as_view(), name="relist-item"),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
